<?php

namespace App\Amazon;

use Illuminate\Database\Eloquent\Model;

class AmazonOffer extends Model
{
    //Для преобразования данных из Mysql и обратно
    protected $casts = [
        'path' => 'array',
        'images' => 'array',
        'sizes' => 'array',
        'colors' => 'array',
        'content' => 'array',
    ];

    //Не используем такие колонки
    public $timestamps = false;

    //Какие колонки автоматически заполняются
    protected $fillable = ['offer_id', 'path', 'title', 'stars', 'images',
                            'price_current', 'price_old', 'price_discount', 'status',
                            'sizes', 'colors', 'content', 'parsed', 'url', 'created'];
}
