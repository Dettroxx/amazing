<?php

namespace App\Amazon;

use Illuminate\Database\Eloquent\Model;

//Класс модели для категории парсинга амазона
class AmazonCategory extends Model
{
    //Не используем такие колонки
    public $timestamps = false;

    //Массово присвояемые атрибуты
    protected $fillable = [
        'id', 'name', 'url', 'sort'
    ];
}
