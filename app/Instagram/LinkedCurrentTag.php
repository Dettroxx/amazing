<?php

namespace App\Instagram;

use Illuminate\Database\Eloquent\Model;

class LinkedCurrentTag extends Model
{
    //Не используем такие колонки
    public $timestamps = false;

    //Массово присвояемые атрибуты
    protected $fillable = [
        'post_id', 'tag_id'
    ];
}
