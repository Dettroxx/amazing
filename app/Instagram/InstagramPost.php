<?php

namespace App\Instagram;

use Illuminate\Database\Eloquent\Model;

class InstagramPost extends Model
{
    //Для преобразования данных из Mysql и обратно
    protected $casts = [
        'current_tags' => 'array',
    ];

    //Не используем такие колонки
    public $timestamps = false;

    //Массово присвояемые атрибуты
    protected $fillable = [
        'id', 'code', 'caption', 'url', 'thumb', 'image', 'created', 'deleted', 'commented', 'current_tags', 'current_title', 'freeze_current_title', 'inout'
    ];
}
