<?php

namespace App\Instagram;

use Illuminate\Database\Eloquent\Model;

class PostOfferLink extends Model
{
    //Не используем такие колонки
    public $timestamps = false;

    //Присвояемые иконки
    protected $fillable = [
        'tag_id', 'offer_id'
    ];
}
