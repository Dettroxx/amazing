<?php

namespace App\Instagram;

use Illuminate\Database\Eloquent\Model;

class InstagramTag extends Model
{
    //Не используем такие колонки
    public $timestamps = false;

    //Массово присвояемые атрибуты
    protected $fillable = [
        'id', 'tag'
    ];
}
