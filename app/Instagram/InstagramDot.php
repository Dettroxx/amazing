<?php

namespace App\Instagram;

use Illuminate\Database\Eloquent\Model;

class InstagramDot extends Model
{
    //Не используем такие колонки
    public $timestamps = false;

    //Присвояемые иконки
    protected $fillable = [
        'x', 'y', 'width', 'icon'
    ];
}
