<?php

namespace App\Instagram;

use Illuminate\Database\Eloquent\Model;

class InstagramUser extends Model
{
    //Не используем такие колонки
    public $timestamps = false;

    //Массово присвояемые атрибуты
    protected $fillable = [
        'id', 'user_name', 'user_pic',
    ];
}
