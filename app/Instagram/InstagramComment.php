<?php

namespace App\Instagram;

use Illuminate\Database\Eloquent\Model;

class InstagramComment extends Model
{
    //Не используем такие колонки
    public $timestamps = false;

    //Массово присвояемые атрибуты
    protected $fillable = [
        'id', 'post_id', 'user_id', 'text', 'created'
    ];
}
