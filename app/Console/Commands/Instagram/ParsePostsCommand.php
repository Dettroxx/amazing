<?php

namespace App\Console\Commands\Instagram;

use Illuminate\Console\Command;
use App\Libraries\Instagram\InstagramConfig;
use App\Libraries\Instagram\InstagramParse;

class ParsePostsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'instagram:parse_posts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Парсит посты инстаграмма';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $config = new InstagramConfig("instagram");
        $parser = new InstagramParse($config);
        $parser->scanInstagram();
    }
}
