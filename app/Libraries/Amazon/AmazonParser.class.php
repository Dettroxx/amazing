<?php

namespace App\Libraries\Amazon;

use GuzzleHttp;
use phpQuery;

//Класс, выполняющий логику парсинга amazon
class AmazonParser {
    public function __construct() {

    }


    //Функция для парсинга страницы
    public function parsePage($url) {
        $content = $this->getContent($url);
        $data = $this->getData($content);
        return $data;
    }


    //Функция для получения контента страницы (здесь можно использовать прокси)
    private function getContent($url) {
        $client = new GuzzleHttp\Client();
        $res = $client->request('GET', $url);

        //Здесь должна быть проверка на ошибки
        //$res->getStatusCode();

        return $res->getBody();
    }


    //Функция для получения "чистых" сырых данных страницы на основе спарсенного контента
    private function getData($content) {
        $doc = phpQuery::newDocumentHTML($content, 'utf-8');

        //Получаем данные
        $id = $this->parse_attribute_id($doc);
        $path = $this->parse_attribute_path($doc);
        $title = $this->parse_attribute_title($doc);
        $stars = $this->parse_attribute_stars($doc);
        $images = $this->parse_attribute_images($doc, $content);
        $prices = $this->parse_attribute_price($doc);
        $status = $this->parse_attribute_status($doc);
        $sizes = $this->parse_attribute_sizes($doc);
        $colors = $this->parse_attribute_colors($doc, $content);
        $content = $this->parse_attribute_content($doc);

        //Собираем конечные данные
        $data = array(
            "offer_id" => $id,
            "path" => $path,
            "title" => $title,
            "stars" => $stars,
            "images" => $images,
            "status" => $status,
            "sizes" => $sizes,
            "colors" => $colors,
            "content" => $content
        );

        //Собираем данные по прайсам
        if (!empty($prices)) {
            if (isset($prices["old"]) && !empty($prices["old"])) $data["price_old"] = $prices["old"];
            if (isset($prices["current"]) && !empty($prices["current"])) $data["price_current"] = $prices["current"];
            if (isset($prices["discount"]) && !empty($prices["discount"])) $data["price_discount"] = $prices["price_discount"];
        }

        return $data;
    }


    //Функция для получения ID товара
    private function parse_attribute_id($doc) {
        $trs = $doc->find("#productDetails_detailBullets_sections1 tr");
        $id = "";

        foreach ($trs as $item) {
            $el = pq($item);
            $key = trim($el->find("th")->text());
            if ($key != "ASIN") continue;

            $value = trim($el->find("td")->text());
            $id = $value;
        }

        return $id;
    }


    //Функция для получения списка категорий
    private function parse_attribute_path($doc, $content = "") {
        //Полный путь категории товара
        $path = $doc->find("#wayfinding-breadcrumbs_feature_div li a");
        $path_array = array();
        foreach ($path as $element) {
            $el = pq($element);
            $path_array[] = trim($el->html());
        }

        return $path_array;
    }


    //Функция для получения названия продукта
    private function parse_attribute_title($doc, $content = "") {
        $title = trim($doc->find("#productTitle")->text());
        return $title;
    }


    //Функция для получения рейтинга продукта
    private function parse_attribute_stars($doc, $content = "") {
        $stars = $doc->find("#dp-container #averageCustomerReviews_feature_div #acrPopover")->attr("title");
        preg_match('/\d*\.\d*/s', $stars, $matches);

        //Проверка есть - ли рейтинг
        if (isset($matches[0])) {
            return $matches[0];
        } else {
            return "";
        }
    }


    //Функция для поучения списка изображений
    private function parse_attribute_images($doc, $content = "") {
        //Сначала получаем контент самого скрипта со списком изображений
        preg_match('/register\("ImageBlockATF.*?<\/script>/s', $content, $matches);

        //Если вообще ничего нет возвращаем пустой массив
        if (!isset($matches[0])) {
            return array();
        }
        $imgContent = $matches[0];

        //Получаем стандартные изображения (hiRes, thumb, large)
        $getImgs = array("hiRes", "thumb", "large");
        $imgData = array();

        foreach ($getImgs as $key) {
            //Получаем hiRes-изображения
            preg_match_all('/("'.$key.'":\s*")(.*?)(\",)/', $imgContent, $matches, PREG_SET_ORDER);

            $prepared_array = array();
            foreach ($matches as $item) {
                $prepared_array[] = $item[2];
            }

            //Добавляем результат в массив результатов
            $imgData[$key] = $prepared_array;
        }

        //Обрабатываем массив данных для группировки по значению
        $count = count($imgData["hiRes"]);
        $result_imgs = array();
        for ($i = 0; $i < $count; $i++) {
            $result_imgs[] = array(
                "thumb" => $imgData["thumb"][$i],
                "medium" => $imgData["large"][$i],
                "big" => $imgData["hiRes"][$i],
            );
        }

        return $result_imgs;
    }


    //Функция для парсинга стоимости
    private function parse_attribute_price($doc, $content = "") {
        $price_area = $doc->find("#price tr");

        //Обходим список цен
        $prices = array();
        foreach ($price_area as $item) {
            $elements = (pq($item))->find("td");
            $keys = $elements->eq(0);
            $values = $elements->eq(1);


            //Формируем сырой массив цен
            $count = count($keys);
            for ($i = 0; $i < $count; $i++) {
                $key = pq($keys[$i]);
                $key = trim($key->text());
                $key = strtolower($key);

                $value = pq($values[$i]);
                $value = trim($value->text());
                $value = strtolower($value);

                //Заносим элементы
                $prices[$key] = $value;
            }
        }

        //Обработка цен (выборка)
        $prices_prepared = array();
        foreach ($prices as $key => $value) {
            switch ($key) {
                case "was:": //Старая цена
                    $pattern = '/(\$)([\d.]*)/s';
                    preg_match($pattern, $value, $matches);
                    if (isset($matches[2])) {
                        $prices_prepared["old"] = doubleval($matches[2]);
                    }
                    break;


                case "price:": //Текущая цена
                    $pattern = '/(\$)([\d.]*)/s';
                    preg_match($pattern, $value, $matches);
                    if (isset($matches[2])) {
                        $prices_prepared["current"] = doubleval($matches[2]);
                    }
                    break;


                case "you save:": //Скинка (в % и цена)
                    $pattern = '/(\$)([\d.]*)\s*\((\d)*%\)/s';
                    preg_match($pattern, $value, $matches);
                    if (isset($matches[2])) { //Сколько сохраняет
                        $prices_prepared["saved"] = doubleval($matches[2]);
                    }
                    if (isset($matches[3])) { //Сколько процентов сохраняем
                        $prices_prepared["discount"] = doubleval($matches[3]/100);
                    }
                    break;
            }
        }

        return $prices_prepared;
    }


    //Функция для получения статуса товара
    private function parse_attribute_status($doc, $content = "") {
        $status = trim(($doc->find("#availability .a-size-medium"))->text());
        return $status;
    }


    //Функция для получения списка размеров
    private function parse_attribute_sizes($doc) {
        $sizes = $doc->find("#variation_size_name li .a-size-base");
        $sizes_array = array();
        foreach ($sizes as $item) {
            $el = pq($item);
            $sizes_array[] = $el->text();
        }

        if (!empty($sizes_array)) {
            return $sizes_array;
        } else {
            return array();
        }
    }


    //Функция для парсинга цветов товара
    private function parse_attribute_colors($doc, $content) {
        //Получаем текстовые описание цветов
        $sizes = $doc->find("#variation_color_name img");
        $sizes_array = array();
        foreach ($sizes as $item) {
            $el = pq($item);
            $sizes_array[] = $el->attr("alt");
        }

        if (empty($sizes)) {
            return array();
        } else {
            return $sizes_array;
        }
    }


    //Функция для получения контента товара
    private function parse_attribute_content($doc) {
        $content = $doc->find("#featurebullets_feature_div li");
        $content_array = array();

        foreach ($content as $item) {
            $el = pq($item);
            $id = trim($el->attr("id"));
            if (!empty($id)) continue;

            $content_array[] = trim($el->find(".a-list-item")->text());
        }

        return $content_array;
    }
}