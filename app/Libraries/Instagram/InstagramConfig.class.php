<?php

namespace App\Libraries\Instagram;

//Класс, инкапсулирующий в себе конфигурацию
class InstagramConfig {
    private $config_name;
    private $tags = [];
    private $tags2 = [];
    private $accounts = [];
    private $maxComments = 0;

    /**
     * @return string[] Возвращает список тегов
     */
    public function getTags() {
        return $this->tags;
    }


    /**
     * @return string[] Возвращает список вторичных тегов
     */
    public function getSecondTags() {
        return $this->tags2;
    }

    /**
     * @return mixed[] Возвращает один аккаунт для авторизации пользователя в инстраграм
     */
    public function getCurrentAccount() {
        return $this->accounts[0];
    }


    /*
     * Возвращает максимальное количество комментов для парсинга
     */
    public function getMaxComments() {
        return $this->maxComments;
    }

    /**
     * Создает конфигурацию для парсера instagram
     * @param mixed[] $config
     */
    public function __construct($configname) {
        $this->config_name = $configname;
        $this->tags = $this->getConfig("tags", array());
        $this->tags2 = $this->getConfig("tags2", array());
        $this->accounts = $this->getConfig("accounts", array());
        $this->maxComments = $this->getConfig("maxComments");
    }


    //Функция - фасад для получения конфигурации
    private function getConfig($name, $default = false) {
        $data = config($this->config_name.".".$name);
        if (empty($data)) {
            if ($default) {
                return $default;
            } else {
                return $data;
            }
        } else {
            return $data;
        }
    }
}