<?php

namespace App\Libraries\Instagram;

use App\Instagram\InstagramComment;
use App\Instagram\InstagramTag;
use App\Instagram\InstagramPost;
use App\Instagram\InstagramTagLink;
use App\Instagram\InstagramUser;
use \InstagramScraper\Instagram;
use InstagramScraper\Model\Comment;
use InstagramScraper\Model\Media;

//Класс, отвечающий за парсинг инстаграм файлов
class InstagramParse {
    private $config = null;
    private $cache_folder = "";

    /**
     * Создает парсер с переданной конфигурацией
     * @param InstagramConfig $config
     */
    public function __construct($config) {
        $this->config = $config;
        $this->cache_folder = storage_path("/libs/instagram/cache");
    }


    //Новый объект типа instagram для запроса
    private function getInstagram() {
        $config = $this->config;
        $account = $config->getCurrentAccount();
        $instagram = Instagram::withCredentials($account['username'], $account['password'], $this->cache_folder);
        $instagram->login();
        return $instagram;
    }


    //Производит сканирование instagram
    public function scanInstagram() {
        $instagram = $this->getInstagram();

        $medias = $instagram->getCurrentTopMediasByTagName('interiordesign');

        //Обрабатываем данные
        $totalElements = array();
        foreach ($medias as $media) {
            if ($this->baseNotValid($media)) continue;

            //Получаем обработанный элемент данных
            $element = $this->handleOne($media);

            //Проверяем валидность уже второй раз
            $intersects = $this->intersectsTags($element);
            if ($this->notTagsValid($intersects)) continue;

            //Иначе добавлеяем теги, по которым он пересекается в список
            $element["intersects"] = $intersects;

            //Делаем заполнение элемента в БД (в случае каких - либо критериев он может сам по себе отпасть)
            $totalElements[] = $element;
        }

        //Производим заполнение данных в БД
        $this->fillElements($totalElements);
    }


    /***
     * Проверяем базовую валидность
     * @param $media
     * @return bool Валиден элемент или нет
     */
    private function baseNotValid($media) {
        $type = $media->getType();
        if ($type == "video") {
            return true;
        } else {
            return false;
        }
    }


    /**
     * Производит генерацию одного элемента с жесткой структурой на основе полученных данных
     * @param Media $item
     */
    private function handleOne($item) {
        $data = array();

        //$id элемента
        $id = $item->getId();
        $data["id"] = $id;

        //Получаем код элемента
        $code = $item->getShortCode();
        $data["code"] = $code;

        //Время создания
        $created = $item->getCreatedTime();
        $data["created"] = $created;

        //Заголовок
        $caption = $item->getCaption();
        $data["caption"] = $item->getCaption();

        //Теги
        $tags = $this->fetchTags($caption);
        $data["tags"] = $tags;

        //Полный путь
        $url = $item->getLink();
        $data["url"] = $url;

        //Получаем изображения
        $image = $item->getImageStandardResolutionUrl();
        $data["image"] = $image;

        //Получаем маленькое изображение
        $thumb = $item->getImageLowResolutionUrl();
        $data["thumb"] = $thumb;

        return $data;
    }


    /**
     * @param $caption Функция, создающая теги на основе заголовка
     */
    private function fetchTags($caption) {
        $regex = '/(?:#)([А-яA-z0-9_](?:(?:[А-яA-z0-9_]|(?:\.(?!\.))){0,28}(?:[А-яA-z0-9_]))?)/u';
        preg_match_all($regex, $caption, $out, PREG_SET_ORDER);

        //Производим обработку
        $tags = array();
        foreach ($out as $item) {
            $tags[] = $item[1];
        }

        return $tags;
    }


    /***
     * Проверяет элемент на валидность тегов. Если не валидны - выходим
     * @param $item
     */
    private function notTagsValid($intersects) {
        //Проверяем наличие
        if (count($intersects) > 0) {
            return false;
        } else {
            return true;
        }
    }


    /***
     * Находит список тегов, который совпадают с тегами конфигурации
     * @param $item
     * @return array
     */
    private function intersectsTags($item) {
        //Получаем значения тегов
        $tags_mask = $this->config->getSecondTags();
        $tags = $item["tags"];

        //Делаем уникализацию тегов
        $tags_mask = array_unique($tags_mask);
        $tags = array_unique($tags);

        //Находим массив пересечений по тегам
        $intersects = array_uintersect($tags_mask, $tags, "strcasecmp");
        return $intersects;
    }


    //Функция, которая производит заполнение данных в базу по спарсенным элементам
    private function fillElements($elements) {
        foreach ($elements as $post) {
            //Заполняем список тегов
            $tags = $post["intersects"];
            $this->fillTags($tags);

            //Заполняем список связей тегов с постами
            $id = $post["id"];
            $this->fillTagsLinks($id, $tags);

            //Заполняем информацию о посте
            $this->fillPost($post);
        }
    }


    /***
     * Функция, заполняющая основную информацию о посте
     * @param $post
     */
    private function fillPost($post) {
        //Смотрим, есть - ли пользователь с таким id
        $id = $post["id"];
        $count = InstagramPost::where('id', '=', $id)->count();
        if ($count == 0) {
            InstagramPost::create($post);
        }
    }



    /***
     * Функция, производит заполнение списка тегов
     * @param $tags
     */
    private function fillTags($tags) {
        foreach ($tags as $tag) {
            //Смотрим, есть - ли пользователь с таким id
            $count = InstagramTag::where('tag', '=', $tag)->count();
            if ($count == 0) {
                InstagramTag::create(array("tag" => $tag));
            }
        }
    }


    /***
     * Функция, заполняющая тегами информацию
     * @param $id
     * @param $tags
     */
    private function fillTagsLinks($id, $tags) {
        $tag_ids = InstagramTag::whereIn('tag', $tags)->pluck('id')->toArray();

        //Проходим по всем тегам в листе и производим генерацию связей
        foreach ($tag_ids as $tag_id) {
            $count = InstagramTagLink::where([
                "post_id" => $id,
                "tag_id" => $tag_id
            ])->count();

            if ($count == 0) {
                InstagramTagLink::create(array(
                    "post_id" => $id,
                    "tag_id" => $tag_id
                ));
            }
        }
    }


    /***
     * Функция, которая производит парсинг комментариев для одного поста
     */
    public function parseComments() {
        $instagram = $this->getInstagram();
        $ids = $this->getIdsForComments();

        //Производим парсинг комментариев для постов из списка
        $total_comments = array();
        foreach ($ids as $id) {
            try {
                $comments = $this->parseCommentsForPost($id, $instagram);
            } catch(\Exception $e) {
                //Если произошла ошибка и пост был удален - просто ставим что закомменчено
                InstagramPost::where('id', '=', $id)->update(['commented' => true]);
                continue;
            }

            if (empty($comments)) {
                $comments = array();
            }
            $total_comments[$id] = $comments;

            //Осуществляем небольшую задержку перед получением следующей информации о комментах
            $this->delay();
        }

        //Обновление комментариев в базе данных
        $this->updateDBComments($total_comments);
    }


    //Функция, которая проходится по списку комментариев и производит их обновление в базе данных
    private function updateDBComments($comments_total) {
        //Проходимся по списку постов
        foreach ($comments_total as $post_key => $comments) {
            //Проходимся по списку комментариев
            foreach ($comments as $comment) {
                //Производим препарирование одного комментария для базы данных
                $this->updateDBComment($comment);
            }

            //Получаем объект с post_id равный текущему и ставим ему галочку что комменты обновлены
            InstagramPost::where('id', '=', $post_key)->update(['commented' => true]);
        }
    }


    //Функция, производящая обновление одного комментария
    private function updateDBComment($comment) {
        if (is_null($comment)) return;

        //Сначала создаем пользователя
        $user_data = array(
            "id" => $comment["user_id"],
            "user_name" => $comment["user_name"],
            "user_pic" => $comment["user_pic"],
        );

        //Обновляем данные по комментариям
        $this->createOrUpdateInstagramUser($user_data);

        //Формируем данные для заполнения объекта комментария
        $comment_data = array(
            "id" => $comment["comment_id"],
            "post_id" => $comment["post_id"],
            "created" => $comment["created"],
            "user_id" => $comment["user_id"],
            "text" => $comment["text"]
        );

        //Обновляем данные по комментариям
        $this->createOrUpdateInstagramComment($comment_data);
    }


    //Функция, которая производит парсинг комментариев для одного поста
    private function parseCommentsForPost($id, $instagram) {
        $maxComments = $this->config->getMaxComments();
        $comments = $instagram->getMediaCommentsById($id, $maxComments);

        $prepared_comments = array();
        foreach ($comments as $comment) {
            $prepared_comment = $this->prepareOneComment($comment, $id);
            $prepared_comments[] = $prepared_comment;
        }

        return $prepared_comments;
    }


    /***
     * Функция, производящая обработку комментария
     * @param Comment $comment
     */
    private function prepareOneComment($comment, $id) {
        $comment_data = array();

        //id поста
        $comment_data["post_id"] = $id;

        //id комментария
        $comment_id = $comment->getId();
        $comment_data["comment_id"] = $comment_id;

        //Дата создания комментария
        $created = $comment->getCreatedAt();
        $comment_data["created"] = $created;

        //Получаем информацию о создателе комментария
        $owner_data = array();
        $owner = $comment->getOwner();
        $owner_data["user_id"] = $owner->getId();
        $owner_data["user_name"] = $owner->getUsername();
        $owner_data["user_pic"] = $owner->getProfilePicUrl();

        //Получаем текст комментария с предварительной обработкой
        $text = $this->prepareCommentText($comment->getText(), $owner_data);

        //Добавляем текст в данные
        $comment_data["text"] = $text;

        //Дополняем информацию о комментарии информацией о создателе
        $data = array_merge($comment_data, $owner_data);

        //Возвращаем
        return $data;
    }


    //Функция, которая производит обновление информации о пользователе (если такого еще не произошло)
    private function createOrUpdateInstagramUser($owner_data) {
        //Смотрим, есть - ли пользователь с таким id
        $id = $owner_data["id"];
        $count = InstagramUser::where('id', '=', $id)->count();
        if ($count > 0) {
            //Обновляем информацию\
            $object = InstagramUser::where('id', '=', $id)->first();
            $object->fill($owner_data);
            $object->save();
        } else {
            //Создаем нового пользователя
            InstagramUser::create($owner_data);
        }

        //Заполняем информацию о пользователе
        InstagramUser::updateOrCreate($owner_data);
    }


    //Функция, которая производит добавление информации о комментарии (и обновление его)
    private function createOrUpdateInstagramComment($comment_data) {
        //Смотрим, есть - ли пользователь с таким id
        $id = $comment_data["id"];
        $count = InstagramComment::where('id', '=', $id)->count();
        if ($count > 0) {
            //Обновляем информацию\
            $object = InstagramComment::where('id', '=', $id)->first();
            $object->fill($comment_data);
            $object->save();
        } else {
            //Создаем нового пользователя
            InstagramComment::create($comment_data);
        }

        //Заполняем информацию о пользователе
        InstagramComment::updateOrCreate($comment_data);
    }


    //Функция, производящая постобработку данных о комментарии
    private function prepareCommentText($text, $owner_data) {
        return $text;
    }


    //Функция, которая получает список постов, для которых необходимо произвести парсинг комментариев
    private function getIdsForComments() {
        //Получаем id нескольких постов, отсортированных по времени
        $limit = 2;
        $post_ids = InstagramPost::where("commented", "=", false)
                                   ->where("deleted", "=", false)
                                   ->orderBy("created", "asc")->limit($limit)->pluck("id")->toArray();

        return $post_ids;
    }


    //Функция, осуществляющая задержку перед получением следующей порции данных
    private function delay() {
        //Осуществляет задержку в случайном порядке на определенное количество миллисекунд (от 50 до 250)
        $ms = rand(5, 250);
        $mks = $ms*1000;
        $mks_offset = rand(0, 300);
        $total_mks = $mks+$mks_offset;
        usleep($total_mks);
    }
}