<?php

namespace App\Http\Controllers\Instagram;

use App\Instagram\InstagramComment;
use Illuminate\Http\Request;
use App\Instagram\InstagramPost;
use App\Http\Controllers\Controller;
use App\Config\TagCategory;
use App\Config\Tags;
use App\Instagram\LinkedCurrentTag;
use DB;
use App\Instagram\InstagramTag;
use App\Instagram\InstagramTagLink;
use App\Instagram\InstagramDot;
use Illuminate\Validation\Rules\In;
use App\Instagram\PostOfferLink;

class GetDataInstagramController extends Controller
{
    //Возвращает данные для JSON - представления
    public function load() {
        $query = $data = InstagramPost::select("code", "thumb");

        //Вот здесь уже устанавливаем свои сложные условия
        $condition = empty(request("condition")) ? [] : json_decode(request("condition"), true);

        //Применяем условия к запросу к базе данныех
        $this->applyConditions($query, $condition);

        $data = $query->get()->toArray();
        return response()->json($data);
    }


    //Добавляет новую связь для offer
    public function addNewOfferLink($code, Request $request) {
        $post = InstagramPost::where("code", "=", $code)->first();
        $id = $post->id;
        $tag = $request->get("tag");
        $offer = $request->get("offer");
        if (empty($tag) || empty($offer) || empty($id)) return;

        //Иначе создаем новую связь
        $link = new PostOfferLink();
        $link->tag_id = $tag;
        $link->offer_id = $offer;
        $link->save();

        return array("success" => true);
    }


    //Возвращает список всех офферов вообще возможных
    public function getAllOffers() {
        //Получаем список связанных офферов;
        $offers = (array) DB::table('amazon_offers')
            ->select()
            ->get()
            ->toArray();

        //Проходимся по списку офферов и формируем словарь по типу ключей
        $prepared_offers = array();
        foreach ($offers as &$offer) {
            //Производим подготовку offer;
            $offer = (array) $offer;
            $offer["path"] = json_decode($offer["path"], true);
            $offer["images"] = json_decode($offer["images"], true);
            $offer["sizes"] = json_decode($offer["sizes"], true);
            $offer["colors"] = json_decode($offer["colors"], true);
            $offer["content"] = json_decode($offer["content"], true);
            $prepared_offers[] = $offer;
        }

        return $prepared_offers;
    }



    //Функция, которая производит удаление одной ссылки - связи
    public function deleteOfferLink($id) {
        DB::table('post_offer_links')
        ->where("id", $id)
        ->delete();

        //Возвращаем успешный результат
        return array("success" => true);
    }


    //Функция для обновления одного тега
    public function updateDot($code, Request $request) {
        $post = InstagramPost::where("code", "=", $code)->first();
        $id = $post->id;
        $tag = $request->get("tag");
        if (empty($tag)) return;

        //Обновляем тег
        $dot = InstagramDot::find($tag["id"]);
        if (!empty($dot)) {
            $dot->fill($tag);
            $dot->save();
        }


        return $tag;
    }


    //Получает для текущего тега список всех связанных с ним офферов
    public function getOffersLinksRaw($id, Request $request) {
        $post = InstagramPost::where("code", "=", $id)->first();
        $id = $post->id;
        $tag = $request->get("tag");

        //Получаем список связанных офферов;
        $offers = (array) DB::table('post_offer_links')
            ->select([
                "post_offer_links.id as link_tag_id",
                "post_offer_links.*",
                "amazon_offers.*",
            ])
            ->where("post_offer_links.tag_id", "=", $tag)
            ->join('amazon_offers', 'post_offer_links.offer_id', '=', 'amazon_offers.id')
            ->get()
            ->toArray();

        //Проходимся по списку офферов и формируем словарь по типу ключей
        $prepared_offers = array();
        foreach ($offers as &$offer) {
            //Производим подготовку offer;
            $offer = (array) $offer;
            $offer["path"] = json_decode($offer["path"], true);
            $offer["images"] = json_decode($offer["images"], true);
            $offer["sizes"] = json_decode($offer["sizes"], true);
            $offer["colors"] = json_decode($offer["colors"], true);
            $offer["content"] = json_decode($offer["content"], true);
            $prepared_offers[] = $offer;
        }

        return $prepared_offers;
    }


    //Функция, которая производит добавление списка точек в базу данных
    public function addDots($code, Request $request) {
        $post = InstagramPost::where("code", "=", $code)->first();
        $id = $post->id;
        $tags = $request->get("tags");
        if (empty($tags)) {
            $tags = array();
        }


        //Проходим по массиву данных
        foreach ($tags as &$tag) {
            $dot = new InstagramDot();
            $dot->post_id = $id;
            $dot->fill($tag);
            $dot->save();

            //Сохраняем id элемента теперь
            $tag["id"] = $dot->id;
        }

        return $tags;
    }


    //Функция, которая производит удаление одного тега
    public function deleteDot($code, Request $request) {
        $post = InstagramPost::where("code", "=", $code)->first();
        $id = $post->id;
        $tag_id = $request->get("tag_id");

        //Производим удаление текущего тега
        DB::table('instagram_dots')
        ->where("post_id", "=", $id)
        ->where("id", "=", $tag_id)
        ->delete();

        //Производим удаление всех связанных офферов
        //TODO Здесь надо произвести удаление связей офферов

        return $tag_id;
    }

    //Функция для применения сложных условий к базе данных
    private function applyConditions($query, $condition) {
        //Устанавливаем количество элементов, которое необходимо взять
        if (!empty($condition["limit"] && is_numeric($condition["limit"]))) {
            $limit = intval($condition["limit"]);
        } else {
            $limit = 20;
        }
        $query->take($limit);


        //Устанавливаем значение, которое необходимо пропустить от начала
        if (!empty($condition["offset"] && is_numeric($condition["offset"]))) {
            $offset = intval($condition["offset"]);
        } else {
            $offset = 0;
        }
        $query->skip($offset);


        //Устанавливаем базовую сортировку
        $query->orderby("created", "desc");
    }


    //Функция для получения одного элемента
    public function getElement($id) {
        $post = InstagramPost::where("code", "=", $id)->first();
        $id = $post->id;
        $data = $post->toArray();

        //Получаем список спарсенных тегов
        $data["instagram_tags"] = $this->getInstaParsedTags($id);

        //Получаем список всех доступных тегов
        $data["instagram_tags_total"] = $this->getInstagramTagsTotal($id);

        //Получаем список всех связанных комментариев с постом
        $data["instagram_comments"] = $this->getInstagramComments($id);

        //Список связанных точек на фотографии
        $data["offers_links"] = $this->getOffersLinks($id);

        return response()->json($data);
    }


    //Функция, которая получает связанные точки на фотографии
    public function getOffersLinks($id) {
        //Пока просто выводим список точек
        $dots = (array) DB::table('instagram_dots')
        ->select()
        ->where("post_id", "=", $id)
        ->get()
        ->toArray();

        //Проходимся по всем элементам и собираем id точек
        $ids = array();
        foreach ($dots as $dot) {
            $ids[] = $dot->id;
        }

        //Получаем список связанных офферов;
        $offers = (array) DB::table('post_offer_links')
            ->select([
                "post_offer_links.id as link_tag_id",
                "post_offer_links.*",
                "amazon_offers.*",
            ])
            ->whereIn("tag_id", $ids)
            ->join('amazon_offers', 'post_offer_links.offer_id', '=', 'amazon_offers.id')
            ->get()
            ->toArray();

        //Проходимся по списку офферов и формируем словарь по типу ключей
        $prepared_offers = array();
        foreach ($offers as &$offer) {
            //Производим подготовку offer;
            $offer = (array) $offer;
            $offer["path"] = json_decode($offer["path"], true);
            $offer["images"] = json_decode($offer["images"], true);
            $offer["sizes"] = json_decode($offer["sizes"], true);
            $offer["colors"] = json_decode($offer["colors"], true);
            $offer["content"] = json_decode($offer["content"], true);

            $tag_id = $offer["tag_id"];
            $prepared_offers[$tag_id][] = $offer;
        }

        //Проходимся по каждому элементу и добавляем в него офферы
        $result = array();
        foreach ($dots as &$dot) {
            $current = (array)$dot;

            //Добавляем для каждого список связанных с ним офферов
            $tag_id = $dot->id;

            if (isset($prepared_offers[$tag_id])) {
                $current["offers"] = $prepared_offers[$tag_id];
            } else {
                $current["offers"] = array();
            }


            $result[] = $current;
        }

        return $result;
    }


    //Функция для получения списка комментариев instagram
    private function getInstagramComments($id) {
        $comments = DB::table('instagram_comments')
            ->select([
                "instagram_users.id as user_id",
                "instagram_users.user_name as user_name",
                "instagram_users.user_pic as user_pic",
                "instagram_comments.text as text",
            ])
            ->where("post_id", "=", $id)
            ->join('instagram_users', 'instagram_comments.user_id', '=', 'instagram_users.id')
            ->get();

        return $comments;
    }


    //Получаем список instagram - тегов
    private function getInstagramTagsTotal($id) {
        //Получаем список связанных с ним instagram-тегов
        $tags = DB::table('instagram_tags')
            ->select()
            ->get();

        //Фильтруем список
        $prepared_tags = array();
        foreach ($tags as $item) {
            $prepared_tags[] = array(
                "value" => $item->id,
                "label" => $item->tag
            );
        }

        return $prepared_tags;
    }


    //Получаем список спарсенных инстаграм - тегов
    private function getInstaParsedTags($id) {
        //Получаем список связанных с ним instagram-тегов
        $tags = DB::table('instagram_tag_links')
            ->select()
            ->where("post_id", "=", $id)
            ->join('instagram_tags', 'instagram_tags.id', '=', 'instagram_tag_links.tag_id')
            ->get();

        //Фильтруем список
        $prepared_tags = array();
        foreach ($tags as $item) {
            $prepared_tags[] = array(
                "value" => $item->tag_id,
                "label" => $item->tag
            );
        }

        //Добавляем эти данные
        return $prepared_tags;;
    }


    //Функция для получения списка комментариев
    public function getComments($id) {
        $id = InstagramPost::where("code", "=", $id)->first()->id;

        //Теперь получаем список всех комментариев
        $comments = InstagramComment::where("post_id", "=", $id)->get();

        return response()->json($comments);
    }


    //Функция для получения списка тегов
    public function getPostTags($id) {
        $post = InstagramPost::where("code", "=", $id)->first();

        //Получаем список всех тегов, которые возможны
        $tags = DB::table('tags')
            ->select([
                'tags.id as tag_id',
                'tags.name as tag_name',
                'tag_categories.id as category_id',
                'tag_categories.name as category_name'
            ])
            ->join('tag_categories', 'tags.category_id', '=', 'tag_categories.id')
            ->get();

        //Получаем у текущего ресурса список выбранных тегов
        $id = $post->id;

        //Получаем список связанных тегов с текущим постом instagram
        $tags_current = (array) DB::table('linked_current_tags')
        ->select(["tag_id", "category_id"])
        ->where("post_id", "=", $id)
            ->join('tags', 'tags.id', '=', 'linked_current_tags.tag_id')
        ->get()
        ->toArray();

        //Обработка
        if (!empty($tags_current)) {
            $prepared_tags = array();
            foreach ($tags_current as $item) {
                $prepared_tags[$item->category_id][$item->tag_id] = (array) $item;
            }
            $tags_current = $prepared_tags;
        }

        //Сгруппировываем теги
        $result_tags = array();
        foreach ($tags as $item) {
            $item = (array)$item;
            $result_tags[$item['category_id']]['childs'][$item["tag_id"]] = $item["tag_name"];
            $result_tags[$item['category_id']]['category_name'] = $item['category_name'];
        }


        //Теперь производим модификцацию для "внедрения" уже существующих у текущего элемента тегов
        $error_tags = array();
        foreach ($result_tags as $category_id => &$item) {
            if (!isset($tags_current[$category_id])) {
                $item["model"] = array();
                continue;
            }

            //Получаем список тегов
            $childs = $item["childs"];
            $current_selected = array();

            //Получаем список всеъ доступных тегов
            $current_tag = $tags_current[$category_id];
            $current_list = array();
            foreach ($current_tag as $tag_key => $tag_el) {
                $tag_id = $tag_el["tag_id"];
                if (!isset($childs[$tag_id])) continue;

                //Иначе получаем название
                $current_list[] = array(
                    "value" => $tag_id,
                    "label" => $childs[$tag_id]
                );
            }

            $item["model"] = $current_list;
        }

        //Производим дополнительную предобработку для того, чтобы вернуть childs на место
        $new_resulta = array();
        foreach ($result_tags as $category_id => $item_cur) {
            $childs = array();
            $childs = $item_cur["childs"];

            //Фомируем массив элементов
            $prepared_childs = array();
            foreach ($childs as $child_id => $child_val) {
                $prepared_childs[] = array(
                    "label" => $child_val,
                    "value" => $child_id
                );
            }


            $item_cur["childs"] = $prepared_childs;

            //Добавляем в общий список
            $new_resulta[$category_id] = $item_cur;
        }



        //Возвращаем данные
        $data = array(
            "alltags" => $new_resulta
        );

        return json_encode($data, 384);
    }


    //Функция, которая обновляет данные по посту instagram
    public function updatePost($id, Request $request) {
        $item = $request->get("item");
        if (!empty($item)) $this->updatePostItem($id, $item);

        $tags = $request->get("tags");
        $this->updatePostTags($id, $tags);

        return $id;
    }


    //Функция, которая производит обновление списка тегов
    private function updatePostTags($id, $tags) {
        $post = InstagramPost::where("code", "=", $id)->first();
        $post_id = $post->id;

        //Сначала просто получаем список тегов
        LinkedCurrentTag::where("post_id", "=", $post_id)->delete();

        //Потом формируем список тегов для импорта новых
        $prepared_data = array();
        foreach ($tags as $tag) {
            $prepared_data[] = array(
                "post_id" => $post_id,
                "tag_id" => $tag
            );
        }

        //Вставляем новые теги
        LinkedCurrentTag::insert($prepared_data);
    }


    //Функция, которая обновляет пост instagram
    private function updatePostItem($id, $item) {
        $post = InstagramPost::where("code", "=", $id)->first();

        //Задаем новый заголовок
        if (isset($item["caption"]) && !empty(trim($item["caption"]))) {
            $post->caption = $item["caption"];
        }

        if (isset($item["freeze_current_title"])) {
            $post->freeze_current_title = $item["freeze_current_title"];
        }

        if (isset($item["current_title"])) {
            $post->current_title = $item["current_title"];
        }

        if (isset($item["inout"]) && $item["inout"] > 0 && $item["inout"] < 4) {
            $post->inout = $item["inout"];
        }

        //Сохраняем элемент
        $post->save();
    }
}
