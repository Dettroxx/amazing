<?php

namespace App\Http\Controllers;

use App\Instagram\InstagramPost;
use App\Libraries\Instagram\InstagramParse;
use App\Libraries\Instagram\InstagramConfig;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\In;

class TestController extends Controller
{
    public function instagramParse() {
        //Создаем новый конфиг и парсер для инстаграм
        $config = new InstagramConfig("instagram");
        $parser = new InstagramParse($config);
        $parser->scanInstagram();
    }

    public function parseComments() {
        $config = new InstagramConfig("instagram");
        $parser = new InstagramParse($config);
        $parser->parseComments();
    }

    public function outImages() {
        $images = InstagramPost::where("deleted", "=", false)->orderby("created", "desc")->get()->pluck("thumb")->toArray();
        $data = array(
            'images' => $images
        );
        return view("out_images", $data);
    }
}
