<?php

namespace App\Http\Controllers\Config;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Amazon\AmazonCategory;

class CategoriesConfig extends Controller
{
    public function list() {
        $data = array();
        return view("categories", $data);
    }


    //Функция, которая возвращает список элементов из базы
    public function getList() {
        $data = AmazonCategory::all()->toArray();
        return response()->json($data);
    }


    //Функция, которая производит удаление элемента
    public function remove(Request $request) {
        $id = $request->get("id");
        $flight = AmazonCategory::find($id);
        if ($flight) {
            $flight->delete();
        }
    }


    //Функция, которая производит добавление элемента
    public function add(Request $request) {
        $name = $request->get("name");
        $url = $request->get("url");

        $item = new AmazonCategory();
        $item->name = $name;
        $item->url = $url;
        $item->save();
    }


    //Функция, которая обновляет элементы
    public function update(Request $request) {
        $id = $request->get("id");
        $name = $request->get("name");
        $url = $request->get("url");

        $item = AmazonCategory::find($id);
        if ($item) {
            $item->name = $name;
            $item->url = $url;
            $item->save();
        }
    }
}
