<?php

namespace App\Http\Controllers\Config;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Config\TagCategory;
use App\Config\Tags;

class TagsConfig extends Controller
{
    public function list() {
        $data = array();
        return view("tags", $data);
    }

    //Получает список категорий из базы
    public function getlist_categories() {
        $data = TagCategory::all()->toArray();
        return response()->json($data);
    }


    //Функция, которая производит удаление элемента
    public function remove_categories(Request $request) {
        $id = $request->get("id");
        $flight = TagCategory::find($id);
        if ($flight) {
            $flight->delete();
        }
    }


    //Функция, которая производит добавление элемента
    public function add_categories(Request $request) {
        $name = $request->get("name");

        $item = new TagCategory();
        $item->name = $name;
        $item->save();
    }


    //Функция, которая обновляет элементы
    public function update_categories(Request $request) {
        $id = $request->get("id");
        $name = $request->get("name");

        $item = TagCategory::find($id);
        if ($item) {
            $item->name = $name;
            $item->save();
        }
    }



    //Получает список категорий из базы
    public function getlist_tags(Request $request) {
        $category_id = $request->get("category_id");
        $data = Tags::where("category_id", "=", $category_id)->get()->toArray();
        return response()->json($data);
    }


    //Функция, которая производит удаление элемента
    public function remove_tags(Request $request) {
        $id = $request->get("id");
        $flight = Tags::find($id);
        if ($flight) {
            $flight->delete();
        }
    }


    //Функция, которая производит добавление элемента
    public function add_tags(Request $request) {
        $name = $request->get("name");
        $category_id = $request->get("category_id");

        $item = new Tags();
        $item->name = $name;
        $item->category_id = $category_id;
        $item->save();
    }


    //Функция, которая обновляет элементы
    public function update_tags(Request $request) {
        $id = $request->get("id");
        $name = $request->get("name");

        $item = Tags::find($id);
        if ($item) {
            $item->name = $name;
            $item->save();
        }
    }
}
