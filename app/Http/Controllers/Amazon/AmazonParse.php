<?php

namespace App\Http\Controllers\Amazon;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\Amazon\AmazonParser;
use App\Amazon\AmazonOffer;


class AmazonParse extends Controller
{
    private $parser = null;
    public function __construct(AmazonParser $parser) {
        $this->parser = $parser;
    }

    //Парсит один товар
    public function parse() {
        //Берем один элемент из базы данных, который не был распарсен, парсим его и загоняем данные в базу
        $one = AmazonOffer::where("parsed", "=", false)
                            ->take(1)
                            ->orderby('created', 'ASC')
                            ->first();
        if (empty($one)) return;

        //Иначе парсем и получаем данные
        $url = $one->url;
        if (empty($url)) {
            $one->delete();
            return;
        }

        //Здесь уже получаем распарсенный результат
        $data = $this->parser->parsePage($url);
        $one->fill($data);

        //Ставим поле сохранено
        $one->parsed = true;
        $one->updated = time();

        $one->save();
    }
}
