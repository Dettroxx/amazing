<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    //Массово присвояемые атрибуты
    protected $fillable = [
        'name', 'email', 'password',
    ];

    //Скрываемые поля
    protected $hidden = [
        'password', 'remember_token',
    ];
}
