<?php

namespace App\Config;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    //Не используем такие колонки
    public $timestamps = false;

    //Массово присвояемые атрибуты
    protected $fillable = [
        'id', 'category_id', 'name'
    ];
}
