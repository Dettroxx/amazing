let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//Добавление vendor-css-файлов
mix.styles([
    'node_modules/taggd/dist/taggd.css',
], 'public/css/vendor_custom.css');


//Добавляение vendor-js-файлов
mix.scripts([
    'node_modules/taggd/dist/taggd.min.js',
], 'public/js/vendor_custom.js');


mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');
   

