<?php

Auth::routes();
Route::get('/', 'HomeController@index')->name('home');

//Временный путь для пароверки парсинга instagram
Route::get('/instagram-parse', 'TestController@instagramParse')->name('instagram-parse');
Route::get('/instagram-parse-comments', 'TestController@parseComments')->name('instagram-parse-comments');
Route::get('/images', 'TestController@outImages');


//Для получения списка элементов (маленьких изображений)
Route::get('/get/instagram-list', 'Instagram\GetDataInstagramController@load')
    ->name("instagram-get-data")
    ->middleware("auth");

//Для получения одного элемента
Route::get('/get/instagram-element/{id}', 'Instagram\GetDataInstagramController@getElement')
    ->name("instagram-get-element")
    ->middleware("auth");

//Для получения списка комментариев
Route::get('/get/instagram-comments/{id}', 'Instagram\GetDataInstagramController@getComments')
    ->name("instagram-get-comments")
    ->middleware("auth");


//Временно (для проверки парсинга amazon)
Route::get('/test/amazon-parse', 'Amazon\AmazonParse@parse')
    ->name("amazon-parse-page");



//Конфигурация списка категорий
Route::get('/config/categories', 'Config\CategoriesConfig@list')
    ->name("categories-config")
    ->middleware("auth");

Route::get('/config/categories-getlist', 'Config\CategoriesConfig@getlist')
    ->name("categories-config-getlist")
    ->middleware("auth");

Route::post('/config/categories-remove', 'Config\CategoriesConfig@remove')
    ->name("categories-config-remove")
    ->middleware("auth");

Route::post('/config/categories-add', 'Config\CategoriesConfig@add')
    ->name("categories-config-add")
    ->middleware("auth");

Route::post('/config/categories-update', 'Config\CategoriesConfig@update')
    ->name("categories-config-update")
    ->middleware("auth");


//Конфигурация списка тегов
Route::get('/config/tags', 'Config\TagsConfig@list')
    ->name("tags-config")
    ->middleware("auth");

Route::get('/config/tags-categories-getlist', 'Config\TagsConfig@getlist_categories')
    ->name("tags-categories-getlist")
    ->middleware("auth");

Route::post('/config/tags-categories-remove', 'Config\TagsConfig@remove_categories')
    ->name("tags-categories-remove")
    ->middleware("auth");

Route::post('/config/tags-categories-add', 'Config\TagsConfig@add_categories')
    ->name("tags-categories-add")
    ->middleware("auth");

Route::post('/config/tags-categories-update', 'Config\TagsConfig@update_categories')
    ->name("tags-categories-update")
    ->middleware("auth");


//Список тегов
Route::get('/config/tags-getlist', 'Config\TagsConfig@getlist_tags')
    ->name("tags-getlist")
    ->middleware("auth");

Route::post('/config/tags-remove', 'Config\TagsConfig@remove_tags')
    ->name("tags-remove")
    ->middleware("auth");

Route::post('/config/tags-add', 'Config\TagsConfig@add_tags')
    ->name("tags-add")
    ->middleware("auth");

Route::post('/config/tags-update', 'Config\TagsConfig@update_tags')
    ->name("tags-update")
    ->middleware("auth");



//Для получения списка тегов
Route::get('/get/instagram-post-tags/{id}', 'Instagram\GetDataInstagramController@getPostTags')
    ->name("instagram-get-post-tags")
    ->middleware("auth");

//Для обновления списка тегов
Route::post('/update/instagram-post/{id}', 'Instagram\GetDataInstagramController@updatePost')
    ->name("instagram-update-post")
    ->middleware("auth");

//Для удаления одного тега
Route::post('/delete/instagram-dot/{id}', 'Instagram\GetDataInstagramController@deleteDot')
    ->name("instagram-dot-delete")
    ->middleware("auth");

//Для добавления списка точек
Route::post('/add/instagram-dots/{id}', 'Instagram\GetDataInstagramController@addDots')
    ->name("instagram-dots-add")
    ->middleware("auth");

//Для обновления одного тега
Route::post('/update/instagram-dot/{id}', 'Instagram\GetDataInstagramController@updateDot')
    ->name("instagram-dot-update")
    ->middleware("auth");


//Для получения списка офферов
Route::post('/get/instagram-offer-link/{id}', 'Instagram\GetDataInstagramController@getOffersLinksRaw')
    ->name("instagram-get-offer-link")
    ->middleware("auth");


//Список всех возможных офферов
Route::post('/get/instagram-all-offers', 'Instagram\GetDataInstagramController@getAllOffers')
    ->name("instagram-get-all-offers")
    ->middleware("auth");


//Запрос на открепление одного оффера
Route::post('/delete/instagram-offer-link/{id}', 'Instagram\GetDataInstagramController@deleteOfferLink')
    ->name("instagram-delete-offer-link")
    ->middleware("auth");

//Запрос на открепление одного оффера
Route::post('/add/add-new-offer-link/{id}', 'Instagram\GetDataInstagramController@addNewOfferLink')
    ->name("add-new-offer-link")
    ->middleware("auth");