<?php

return [
    //Общее
    'Login' => 'Войти',
    'Register' => 'Зарегистрироваться',
    'Logout' => 'Выход',

    //Ошибки входа
    'failed' => 'Учетной записи с такими данными авторизации не найдено',
    'throttle' => 'Слишком много попыток входа. Попытайтесь снова через :seconds секунд',

    //Локализация формы входа
    'LoginHeader' => 'Войти в систему',
    'EmailAddress' => 'Email',
    'Password' => 'Пароль',
    'LoginButton' => 'Войти',
    'RememberMe' => 'Запомнить меня',
    'ForgotPassword' => 'Забыли пароль?',

    //Для перевода регистрации
    'RegisterHeader' => 'Регистрация нового пользователя',
    'RegisterName' => 'Логин',
    'RegisterEmail' => 'E-mail',
    'RegisterPassword' => 'Пароль',
    'RegisterConfirmPassword' => 'Подтвердите пароль',
    'RegisterButton' => 'Зарегистрироваться'
];
