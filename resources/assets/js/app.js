
require('./bootstrap');
window.Vue = require('vue');

//Импорт бесконечной прокрутки для левого списка
import infiniteScroll from 'vue-infinite-scroll';
Vue.use(infiniteScroll);

//Добавляем фильтры
Vue.filter('truncate', function (text, stop, clamp) {
    return text.slice(0, stop) + (stop < text.length ? clamp || '...' : '')
})

//Компонент списка
import vSelect from 'vue-select'
Vue.component('v-select', vSelect);
Vue.use(vSelect);

//Компонент даты
Vue.use(require('vue-moment'));

//Регистрируем компоненты.
Vue.component('amazing', require('./components/Amazing.vue'));
Vue.component('images-filter', require('./components/ImagesFilter.vue'));
Vue.component('images-list', require('./components/ImagesList.vue'));
Vue.component('images-big-view', require('./components/ImagesBigView.vue'));
Vue.component('item-form', require('./components/ItemForm.vue'));


//Компоненты конфигурации
Vue.component('categories', require('./components/Categories.vue'));
Vue.component('tags-list', require('./components/Tags.vue'));

//Создаем главный компонент
const app = new Vue({
    el: '#amazing'
});