<?php

use Illuminate\Database\Seeder;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Создает нового пользователя типа "администратор"
        User::create([
            'name' => 'admin',
            'email' => 'admin@amazing-app.ru',
            'password' => Hash::make( 'admin888' )
        ]);
    }
}
