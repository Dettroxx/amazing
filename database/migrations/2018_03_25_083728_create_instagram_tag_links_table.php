<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstagramTagLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instagram_tag_links', function (Blueprint $table) {
            $table->bigInteger('post_id')->unsigned();
            $table->integer('tag_id')->unsigned();

            //Добавляем индекс
            $table->index(['post_id', 'tag_id']);

            //Связи с другими таблицами
            $table->foreign('post_id')->references('id')->on('instagram_posts')->onDelete('cascade');
            $table->foreign('tag_id')->references('id')->on('instagram_tags');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instagram_tag_links');
    }
}
