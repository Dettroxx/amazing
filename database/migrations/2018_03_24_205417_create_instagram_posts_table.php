<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstagramPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instagram_posts', function (Blueprint $table) {
            $table->bigInteger('id')->unsigned()->unique();
            $table->string('code', 16)->unique();
            $table->text('caption');
            $table->string('url', 50);
            $table->text('thumb');
            $table->text('image');
            $table->integer("created")->unsigned();
            $table->boolean("deleted")->default(false);
            $table->boolean("commented")->default(false);

            //Добавляем индекс
            $table->index(['id', 'code', 'created', 'deleted', 'commented']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instagram_posts');
    }
}
