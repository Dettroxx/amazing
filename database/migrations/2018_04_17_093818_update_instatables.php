<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateInstatables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('instagram_posts', function (Blueprint $table) {
            $table->text('current_tags'); //Список выбранных тегов
            $table->string('current_title'); //Текущий заголовок
            $table->boolean('freeze_current_title')->default(false); //Заморозить текущий заголовок (не дает возможность редактировать его)
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instagram_posts', function (Blueprint $table) {
            $table->dropColumn('current_tags');
            $table->dropColumn('current_title');
            $table->dropColumn('freeze_current_title');
        });
    }
}
