<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostOfferLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_offer_links', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tag_id')->unsigned();
            $table->integer('offer_id')->unsigned();

            //Добавляем индекс
            $table->index(['id', 'tag_id', 'offer_id']);

            //Связи с другими таблицами
            $table->foreign('tag_id')->references('id')->on('instagram_dots')->onDelete('cascade');
            $table->foreign('offer_id')->references('id')->on('amazon_offers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_offer_links');
    }
}
