<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmazonOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amazon_offers', function (Blueprint $table) {
            //Базовые данные, которые могут быть использованы для парсинга
            $table->increments('id');
            $table->string('offer_id', 20)->nullable();
            $table->text('path')->nullable();
            $table->string('title', 255)->nullable();
            $table->float('stars', 3, 2)->default(0);
            $table->text('images')->nullable();
            $table->double('price_current')->default(0);
            $table->double('price_old')->default(0);
            $table->double('price_discount')->default(0);
            $table->string('status', 255)->nullable();
            $table->text('sizes')->nullable();
            $table->text('colors')->nullable();
            $table->text("content")->nullable();

            //Атрибуты для того, чтобы узнать что спарсено или нет
            $table->boolean('parsed')->default(false);
            $table->text('url')->nullable();
            $table->integer("created")->default(0);
            $table->integer("updated")->default(0);

            //Добавляем индекс
            $table->index(['id', 'offer_id', 'created', 'parsed']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amazon_offers');
    }
}
