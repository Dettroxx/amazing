<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinkedCurrentTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('linked_current_tags', function (Blueprint $table) {
            $table->bigInteger('post_id')->unsigned();
            $table->integer('tag_id')->unsigned();

            //Добавляем индекс
            $table->index(['post_id', 'tag_id']);

            //Каскады связей
            $table->foreign('post_id')->references('id')->on('instagram_posts')->onDelete('cascade');
            $table->foreign('tag_id')->references('id')->on('tags');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('linked_current_tags');
    }
}
