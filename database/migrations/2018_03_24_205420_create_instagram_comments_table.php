<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstagramCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instagram_comments', function (Blueprint $table) {
            $table->bigInteger('id')->unsigned()->unique();
            $table->bigInteger('post_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->text('text');
            $table->integer("created")->unsigned();

            //Добавляем индекс
            $table->index(['id', 'post_id', 'user_id', 'created']);

            //Связи с другими таблицами
            $table->foreign('post_id')->references('id')->on('instagram_posts')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('instagram_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instagram_comments');
    }
}
